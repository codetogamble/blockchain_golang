package main

import (
	"encoding/json"
	"log"
	"strings"
)

type Message struct {
	Message string
	Array[] int
	Number float32
}


func main(){
	// const jsonStream = 	`[{"Name": "Ed", "Text": "Knock knock."},{"Name": "Sam", "Text": "Who's there?"},{"Name": "Ed", "Text": "Go fmt."},{"Name": "Sam", "Text": "Go fmt who?"},{"Name": "Ed", "Text": "Go fmt yourself!"}]`
// 	const jsonStream = `
// 	{"Message": "Hello", "Array": [1, 2, 3], "Null": null, "Number": 1.234}
// `
	const jsonStream = `
	{"Message": "Hello", "Array": [1, 2, 3], "Number": 1.234, "Null" : null}
`
	log.Println("json is ", jsonStream)
	dec := json.NewDecoder(strings.NewReader(jsonStream))
	for {
		t, err := dec.Token()
		log.Println("err is ", err)
		log.Println("t is ", t)
		if err != nil{
			break
		}
	}
	var m Message
	if err := dec.Decode(&m); err!=nil{
		log.Println("err while decoding is ", err)
	}
	log.Println("m is ", m)
}



// package main

// import (
// 	"encoding/json"
// 	"fmt"
// 	"io"
// 	"log"
// 	"strings"
// )

// func main() {
// 	const jsonStream = `
// 	{"Message": "Hello", "Array": [1, 2, 3], "Null": null, "Number": 1.234}
// `
// 	dec := json.NewDecoder(strings.NewReader(jsonStream))
// 	for {
// 		t, err := dec.Token()
// 		if err == io.EOF {
// 			break
// 		}
// 		if err != nil {
// 			log.Fatal(err)
// 		}
// 		fmt.Printf("%T: %v", t, t)
// 		if dec.More() {
// 			fmt.Printf(" (more)")
// 		}
// 		fmt.Printf("\n")
// 	}
// 



// package main

// import (
// 	"encoding/json"
// 	"fmt"
// 	"log"
// 	"strings"
// )

// func main() {
// 	const jsonStream = `
// 	[
// 		{"Name": "Ed", "Text": "Knock knock."},
// 		{"Name": "Sam", "Text": "Who's there?"},
// 		{"Name": "Ed", "Text": "Go fmt."},
// 		{"Name": "Sam", "Text": "Go fmt who?"},
// 		{"Name": "Ed", "Text": "Go fmt yourself!"}
// 	]
// `
// 	type Message struct {
// 		Name, Text string
// 	}
// 	dec := json.NewDecoder(strings.NewReader(jsonStream))

// 	// read open bracket
// 	t, err := dec.Token()
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Printf("%T: %v\n", t, t)

// 	// while the array contains values
// 	for dec.More() {
// 		var m Message
// 		// decode an array value (Message)
// 		err := dec.Decode(&m)
// 		if err != nil {
// 			log.Fatal(err)
// 		}

// 		fmt.Printf("%v: %v\n", m.Name, m.Text)
// 	}

// 	// read closing bracket
// 	t, err = dec.Token()
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Printf("%T: %v\n", t, t)

// }
